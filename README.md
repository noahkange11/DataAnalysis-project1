# Crime Analysis involving cars

This is an effort to get insites on the correlation between crimes commited and 
the cars that where involved int the crime between the years of 2011 and 2013

## Dependencies

This project has been done using R programming which meams you musth have
installed R and R-studion onto your local machine or server.
The following are the R packages used in this project
- shiny
- shinydashboard
- plyr
- ggplot2
- shinyjs

## Setup

Clone the this repository onto your prefered directory, Open the project in R-
studio.
Make sure you have installed the required packages listed in the Dependencies
section.
Run the project.

